from lxml import etree
from zipfile import ZipFile
import copy
import os
import re
import shutil

from conf import author

# namespace definitions for the target writer format
text_namespace = "urn:oasis:names:tc:opendocument:xmlns:text:1.0"
office_namespace = "urn:oasis:names:tc:opendocument:xmlns:office:1.0"
namespaces = {
    u"text": text_namespace,
    u"table": "urn:oasis:names:tc:opendocument:xmlns:table:1.0",
    u"office": office_namespace
}

EMPHASIS_RE = re.compile(r"<emphasis>(.*)</emphasis>")

name_to_element = dict()

# hold a mapping from a reference id to the reference itself
ref_number_2_ref = {}


def next_number():
    """Generate consecutive numbers"""
    number = 0
    while True:
        number += 1
        yield number


number_generator = next_number()


def move_sample_nodes(sus_root):
    """
    Remove sample nodes from the template, store them in name_to_element
    """
    all_text_style_nodes = sus_root.xpath(
        "//text:p[@text:style-name] | "
        "//text:list[@text:style-name]| "
        "//text:span[@text:style-name]| "
        "//text:ordered-list",
        namespaces=namespaces
    )

    for text_style_node in all_text_style_nodes:
        copied = copy.deepcopy(text_style_node)
        style_name = "{%s}style-name" % text_namespace
        name_to_element[text_style_node.attrib[style_name]] = copied
        text_style_node.getparent().remove(text_style_node)

    all_table_nodes = sus_root.xpath(
        "//table:table", namespaces=namespaces
    )
    for table_node in all_table_nodes:
        table_node.getparent().remove(table_node)


def get_element(style_name):
    """
    Return a new element for the given style name.
    """
    node = name_to_element[style_name]
    element = copy.copy(node)
    return element


def replace_emphasis(src_node, dst_node):
    """
    Replace an <emphasis> source element by a <SuS_5f_Kursiv> target element
    """
    for e in src_node:
        if e.tag != "emphasis":
            dst_node.append(copy.copy(e))
        else:
            italic = get_element("SuS_5f_Kursiv")
            italic.text = e.text
            italic.tail = e.tail
            dst_node.append(italic)


def handle_references(node):
    """
    Replace reference elements

    Replace reference ids by consecutive numbers, keeping a
    mapping from the number to the reference target.
    """
    for e in node:
        if e.tag == "reference":
            number = next(number_generator)
            node.text += "[%s]" % number
            ref_number_2_ref[number] = e.text


def process_paragraph(node, temp_node):
    """Handle <paragraph> nodes."""
    para = get_element("P1")
    handle_references(node)
    para.text = node.text
    replace_emphasis(node, para)
    empty_para = get_element("P1")
    empty_para.text = ""
    children = temp_node.getchildren()
    if children:
        last_child = children[-1]
        if "{%s}style-name" % text_namespace in last_child.attrib:
            style = last_child.attrib["{%s}style-name" % text_namespace]
            if style in ("WW8Num2", "SuS_5f_Quellcode"):
                temp_node.append(empty_para)

    temp_node.append(para)
    empty_para = copy.copy(empty_para)
    temp_node.append(empty_para)


def process_bullet_list(node, temp_node):
    """Handle <bullet_list> nodes."""
    ordered_list = get_element("WW8Num2")
    for c in list(ordered_list):
        ordered_list.remove(c)
    all_bullet_paras = node.xpath("./list_item/paragraph")
    for bullet_para in all_bullet_paras:
        list_elem = etree.Element("{%s}list-item" % text_namespace)
        bullet_element = get_element("SuS_5f_Aufzaehlung_5f_gegliedert")
        bullet_element.text = bullet_para.text
        replace_emphasis(bullet_para, bullet_element)
        list_elem.append(bullet_element)
        ordered_list.append(list_elem)
    temp_node.append(ordered_list)


def process_enum_list(node, temp_node):
    """Handle <ordered_list> nodes."""
    ordered_list = get_element("WW8Num2")
    for c in list(ordered_list):
        ordered_list.remove(c)
    all_enum_paras = node.xpath("./list_item/paragraph")
    for enum_para in all_enum_paras:
        list_elem = etree.Element("{%s}list-item" % text_namespace)
        enum_element = get_element("SuS_5f_Aufzaehlung_5f_nummeriert")
        enum_element.text = enum_para.text
        replace_emphasis(enum_para, enum_element)
        list_elem.append(enum_element)
        ordered_list.append(list_elem)
    temp_node.append(ordered_list)


def get_box_headling_replaced(replacement):
    headline = get_element("SuS_5f_Kastenheadline")
    for e in headline:
        if e.tail == "Kasten-Headline":
            e.tail = replacement
    return headline


def process_literal_block(node, temp_node):
    """Handle <literal_block> nodes."""
    lines = node.text.split("\n")
    is_listing = False
    if len(lines) > 1:
        first_line = lines[0].strip()
        if first_line.startswith("Listing") and lines[1].strip() == "":
            is_listing = True

    if is_listing:
        lines = lines[2:]
        headline = get_box_headling_replaced(first_line)
        temp_node.append(headline)
    else:
        source_code = get_element("SuS_5f_Quellcode")
        source_code.text = ""
        temp_node.append(source_code)
    for line in lines:
        source_code = get_element("SuS_5f_Quellcode")
        indent = len(line) - len(line.lstrip())
        if indent == 0:
            source_code.text = line

        else:
            source_code.text = ""
            text_elem = etree.Element("{%s}s" % text_namespace)
            text_elem.attrib["{%s}c" % text_namespace] = str(indent)
            text_elem.tail = line
            source_code.append(text_elem)
        temp_node.append(source_code)

    if is_listing:
        endline = get_box_headling_replaced("Ende")
        temp_node.append(endline)


def process_figure(node, temp_node):
    """Handle <figure> nodes."""
    file_name = get_element("SuS_5f_Bilddateiname")
    img = node.xpath("./image")[0]
    file_name.text = os.path.basename(img.attrib["uri"])
    temp_node.append(file_name)
    caption = get_element("P7")
    src_caption = node.xpath("./caption")[0]
    caption.text = src_caption.text
    temp_node.append(caption)


def process_authorbio(node, temp_node):
    """Handling for special paragraphs with authorbio attribute"""
    empty_para = get_element("P1")
    empty_para.text = ""
    temp_node.append(empty_para)
    bio = get_element("SuS_5f_Autor_5f_Kurzbiografie")
    text = node.text or ""
    for e in node:
        if e.tag == "reference":
            text += e.text
            text += e.tail
    bio.text = text
    temp_node.append(bio)


def process_section_3(section_node, temp_node):
    """Handling of level 3 sections"""
    title = section_node.xpath("./title")[0]
    section_headline = get_element("SuS_5f_Absatzheadline")
    section_headline.text = title.text
    temp_node.append(section_headline)
    for child in section_node:
        if child.tag == "paragraph":
            if "authorbio" in child.attrib:
                process_authorbio(child, temp_node)
            else:
                process_paragraph(child, temp_node)
        elif child.tag == "bullet_list":
            process_bullet_list(child, temp_node)
        elif child.tag == "enumerated_list":
            process_enum_list(child, temp_node)
        elif child.tag == "literal_block":
            process_literal_block(child, temp_node)
        elif child.tag == "figure":
            process_figure(child, temp_node)
        elif child.tag == "authorbio":
            process_authorbio(child, temp_node)


def process_section_2(section_node, temp_node):
    """Handling of level 2 sections"""
    title = section_node.xpath("./title")[0]
    subhead1 = get_element("SuS_5f_Subhead1")
    subhead1.text = title.text
    temp_node.append(subhead1)
    subhead2 = get_element("SuS_5f_Subhead2")
    para = section_node.xpath("./paragraph")[0]
    subhead2.text = para.text
    temp_node.append(subhead2)
    author_name = get_element("SuS_5f_Autorname")
    author_name.text = "von (%s)" % author
    temp_node.append(author_name)
    section_nodes = section_node.xpath("./section")
    for section_node in section_nodes:
        process_section_3(section_node, temp_node)


def process_top_section(section_node, temp_node):
    """Handling of top level sections"""
    title = section_node.xpath("//title")[0]
    headline = get_element("P6")
    headline.text = title.text
    temp_node.append(headline)
    section_node = section_node.xpath("./section")[0]
    process_section_2(section_node, temp_node)


def append_link_reference(generated_tree, temp_node):
    """Append a list of references"""
    link_reference_header = get_element(
        "SuS_5f_Links_5f_und_5f_Literatur_5f_Headline")
    temp_node.append(link_reference_header)
    for number in sorted(ref_number_2_ref):
        reference = get_element(
            "SuS_5f_Links_5f_und_5f_Literatur_5f_Text")
        reference_id = ref_number_2_ref[number]
        targets = generated_tree.xpath("//target")
        targets = generated_tree.xpath(
            "//target[@names='%s']" % reference_id)
        if targets:
            target = targets[0]
            reference.text = "[%s] %s" % (
                number, target.attrib["refuri"])
        temp_node.append(reference)


def process_tree(generated_tree, sus_src_tree):
    """Process the source document tree"""
    temp_node = etree.Element("temp_node", nsmap=namespaces)
    section_node = generated_tree.xpath("//section")[0]
    process_top_section(section_node, temp_node)
    append_link_reference(generated_tree, temp_node)
    body = sus_src_tree.xpath(
        "//office:body/office:text", namespaces=namespaces)[0]
    for c in temp_node:
        body.append(c)


def rebuild_sus_arch():
    """Unpack the source archive, patch content and create new archive"""
    generated_tree = etree.parse("_build/xml/index.xml")
    base_name = "SuS_Dokumentvorlage_2010.ott"
    sus_dir = os.path.join("_build", "sus")
    if os.path.exists(sus_dir):
        shutil.rmtree(sus_dir, ignore_errors=False, onerror=None)
    os.makedirs(sus_dir)
    orig_file = os.path.join("_static", base_name)
    odt_name = os.path.splitext(base_name)[0] + ".odt"
    work_file = os.path.join(sus_dir, odt_name)
    shutil.copyfile(orig_file, work_file)
    with ZipFile(work_file, "r") as zf:
        zf.extractall(sus_dir)
    content_file = os.path.join(sus_dir, "content.xml")

    sus_src_tree = etree.parse(content_file)
    sus_root = sus_src_tree.getroot()
    move_sample_nodes(sus_root)
    process_tree(generated_tree, sus_src_tree)
    with open(content_file, "w") as f:
        f.write(etree.tostring(sus_src_tree, pretty_print=True))

    with ZipFile(work_file, "w") as zf:
        for root, dirs, files in os.walk(sus_dir):
            for f in files:
                if f != "SuS_Dokumentvorlage_2010.odt":
                    f_name = os.path.join(root, f)
                    arc_name = f_name[len(sus_dir):]
                    zf.write(f_name, arc_name)


if "__main__" == __name__:
    rebuild_sus_arch()
